# gonginxy

use nginx as static http server and reverse proxy to proxy to go api service (use mux to implement)

## How to run

1. execute go api micro service
    - go run main.go
2. boot nginx as local reverse server to forward request to go micro service
    - cd docker && docker compose up -d

## Check the site

- http://localhost => will be served by Nginx static file server path: public/index.html
- http://locahost/api/hello => will be served by Go Mux /api/hello rest api endpoint
- http://locahost/mux_web/ => will be served by Go static file server path: public/web/index.html
