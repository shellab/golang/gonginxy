package api

import (
	"fmt"
	"net/http"
)

type Route struct {
	Method  string
	Pattern string
	Handler http.HandlerFunc
}

var ( // api package internal variable
	routes []Route
)

func init() {
	fmt.Println("Route init")
	register("GET", "/hello", hello)
	fmt.Printf("%+v\n", routes) // %v %+v %#v
}

func register(method string, pattern string, handler http.HandlerFunc) {
	routes = append(routes,
		Route{
			Method:  method,
			Pattern: pattern,
			Handler: handler,
		},
	)
}

func hello(w http.ResponseWriter, r *http.Request) {
	echoString := "helloworld"
	fmt.Println(echoString)
	fmt.Fprintln(w, echoString)
}

func GetRoutes() []Route {
	return routes
}
