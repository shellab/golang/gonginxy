package main

import (
	"fmt"
	"net/http"

	"shellab.io/gonginxy/api"

	"github.com/gorilla/mux"
)

func main() {
	r := createRoute()
	fmt.Println("start to serve http request.")
	// servce
	// 0.0.0.0:8080/web/
	// 0.0.0.0:8080/www/
	// 0.0.0.0:8080/api/hello
	http.ListenAndServe(":8080", r)
}

func createRoute() *mux.Router {
	route := mux.NewRouter()
	defRoutes := api.GetRoutes()
	for _, defRoute := range defRoutes {
		route.PathPrefix("/api/").Path(defRoute.Pattern).Methods(defRoute.Method).Handler(defRoute.Handler)
	}
	// also serve static file
	// 以 public 目錄為 root 去讀 web/ 目錄資料
	// 目錄結構 public/web/index.htm
	route.PathPrefix("/web/").Handler(http.FileServer(http.Dir("public")))
	// 以 public 目錄為 root 去讀 www/ 目錄資料
	// 透過 StripPrefix 將 request 的 www/ strip 掉
	// 目錄結構 public/index.htm
	route.PathPrefix("/www/").Handler(http.StripPrefix("/www/", http.FileServer(http.Dir("public"))))
	return route
}
